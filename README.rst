Sesame Installation Script
==========================

The Sesame projects provides an installation script that will
automatically install Sesame and the Mapping module support library.

First you will need to download the latest version of the installation
script:

::

    mkdir sesame
    cd sesame
    wget https://bitbucket.org/spolstra/sesame_installer/raw/HEAD/setup_sesame.sh
    chmod +x setup_sesame.sh

Invoke the script with the ``-h`` flag to show the usage information:

::

    ./setup_sesame.sh -h
    Installer script for the Sesame framework.

    Download url:                   https://csa.science.uva.nl/download/sesame/
    Default installation directory: ~/opt
    
    Script Options:
        -h             Show this message
        -p DIR         Install into DIR
        -l             List dependencies
        -H             Clone over https (default)
        -s             Clone over ssl (requires authorisation)
        -t             Install from tar file

    
Required packages
-----------------

Before you run the installer you should check if all the required
packages are installed. The ``-l`` flag will show the list of required
packages:

::

    ./setup_sesame.sh -l
    Sesame depends on the following Ubuntu packages:
    
    gcc
    g++
    libtool
    libltdl-dev
    python-networkx
    python-lxml
    python-argparse
    python-setuptools
    libxerces-c-dev
    libperl-dev
    bison
    flex
    xmllint
    convert (imagemagick)
    
    Install these packages on debian based systems with the command:
    
    sudo apt-get install gcc g++ libtool libltdl-dev python-networkx \
     python-lxml python-argparse python-setuptools libxerces-c-dev \
     libperl-dev bison flex libxml2-utils imagemagick
    
    
    NOTE: Installing from the git source also requires these packages:
    
    git
    autoconf
    automake
    
    Install these packages on debian based systems with the command:
    
    sudo apt-get install git autoconf automake

On a Debian based system such as Ubuntu, you can use the suggested ``apt-get install`` commands to install the required packages.

Installation methods
--------------------

To install the most recent version of Sesame you only need to specify
the installation directory:

::

    ./setup_sesame.sh -p /path/to/my/install/directory

Additional parameters for the configure script of sesame are set in
the environment variable ``SES_EXTRA_CONFIG``. To let configure use a
different version of the xercesc library you would use:

::

    export SES_EXTRA_CONFIG="--with-xercesc=/home/myhomedir/local"
