#!/usr/bin/env sh

# Sesame installer shell script

# Script options
set -e  # Exit on failure
set -u  # Exit on use of uninitalised variable

# Installer options
SESAME_TAR=sesamesim-latest.tar.gz
MAPPINGMODULE_TAR=mappingmodule-latest.tar.gz
BASE_URL=https://staff.fnwi.uva.nl/s.polstra/download

# Will contain export to set sesame environment
SESAME_ENV_FILE=sesame.env

print_deps() {
cat << EOF
Sesame depends on the following Ubuntu packages:

gcc
g++
make
libtool
libltdl-dev
python-networkx
python-lxml
python-argparse
python-setuptools
libxerces-c-dev
libperl-dev
bison
flex
xmllint
convert (imagemagick)

Install these packages on debian based systems with the command:

sudo apt-get install gcc g++ make libtool libltdl-dev python-networkx \\
 python-lxml python-argparse python-setuptools libxerces-c-dev \\
 libperl-dev bison flex libxml2-utils imagemagick


NOTE: Installing from the git source also requires these packages:

git
autoconf
automake

Install these packages on debian based systems with the command:

sudo apt-get install git autoconf automake

EOF
}

print_usage() {
cat << EOF
Installer script for the Sesame framework.

Download url:     		$BASE_URL
Default installation directory: $INSTALLDIR

Script Options:
    -h             Show this message
    -p DIR         Install into DIR
    -l             List dependencies
    -H             Clone over https (default)
    -s             Clone over ssl (requires authorisation)
    -t             Install from tar file
EOF
}



# Takes two parameters. The response from a command and error message
# If the response is not zero, print the error message and exit
check() {
    if [ $1 -ne 0 ]
    then
        echo -e "\e[1;31mERROR: $2\e[0m"
        exit 1
    fi
}

# This function downloads the flow components. Requires Internet access
get_and_unpack_packages() {
    WGET_OPTS="--no-check-certificate -q -nc"
    echo "wget $WGET_OPTS $BASE_URL/sesame/$SESAME_TAR"
    wget $WGET_OPTS $BASE_URL/sesame/$SESAME_TAR
    check $? "Cannot download $SESAME_TAR file"
    tar xzf $SESAME_TAR
    SESAMEDIR=$(tar tf $SESAME_TAR | head -1 | sed -e 's/\/.*//')

    echo "wget $WGET_OPTS $BASE_URL/mappingmodule/$MAPPINGMODULE_TAR"
    wget $WGET_OPTS $BASE_URL/mappingmodule/$MAPPINGMODULE_TAR
    check $? "Cannot download $MAPPINGMODULE_TAR file"
    tar xzf $MAPPINGMODULE_TAR
    #MAPPINGMODULEDIR=`basename $MAPPINGMODULE_TAR .tar.gz`
    MAPPINGMODULEDIR=$(tar tf $MAPPINGMODULE_TAR | head -1 | sed -e 's/\/.*//')
}

get_from_repos() {
    export GIT_SSL_NO_VERIFY=1
    # clone mapping module
    test -d mappingmodule/.git || git clone ${GITBASE}/mappingmodule.git
    # clone sesame project
    test -d sesame/.git || git clone ${GITBASE}/sesame.git

    # set sesame and mappingmodule root dirs
    SESAMEDIR="sesame"
    MAPPINGMODULEDIR="mappingmodule"
}

# Runs setup of mappingmodule
# SESAME_ENV_FILE is appended with PYTHONPATH definition
setup_mappingmodule() {
    cd $MAPPINGMODULEDIR

    # PYTHONPATH needs to be set to the install dir otherwise the install
    # will fail.
    # TODO: How to detect lib64 install?
    python_version=`python -V 2>&1 | sed 's/Python \([0-9]\.[0-9]\).*/\1/'`
    location=$INSTALLDIR/lib/python${python_version}/site-packages
    mkdir -p $location

    export PYTHONPATH=$location${PYTHONPATH:+:$PYTHONPATH}

    python setup.py install --prefix $INSTALLDIR

    # Append export to SESAME_ENV_FILE
    echo "export PYTHONPATH=$location\${PYTHONPATH:+:\$PYTHONPATH}" >> ../$SESAME_ENV_FILE
    cd ..
}

# Sesame setup
setup_sesame() {
    # Enter Sesame root
    cd $SESAMEDIR

    # Add environment to env file.
    echo "export PATH=$INSTALLDIR/bin:\$PATH" >> ../$SESAME_ENV_FILE
    # Also needed during installation.
    export PATH=$INSTALLDIR/bin:$PATH


    # If we get the source from git we need to run autogen.sh
    test "x$SOURCE" = "xfromtar" || ./autogen.sh

    ./configure --prefix=$INSTALLDIR ${SES_EXTRA_CONFIG:-}
    # TODO check succes
    make
    make install
    # Run checks

    make check
    cd ..
}

#----------------------------------------------------------------------
# Start of Main
#----------------------------------------------------------------------

# Default installation directory
INSTALLDIR=$HOME/opt

# Defaults
SOURCE="git"
GITPROT="https"

while getopts "hltsp:" OPTION; do
    case $OPTION in
    h)
        print_usage
        exit 0
        ;;
    l)
        print_deps
        exit 0
        ;;
    p)
        INSTALLDIR=$OPTARG
        # TODO need to create this dir?
        ;;
    t)
        SOURCE="fromtar"
        ;;
    s)
        GITPROT="ssl"
        ;;
    H)
        GITPROT="https"
        ;;
    esac
done

echo "Sesame will be installed in $INSTALLDIR"

# Download packages and unpack
case $SOURCE in
    "pubgit")
        echo "public base is not available at this moment."
        exit 1
        GITBASE="ADD_PUBLIC base here"
        # get_from_repos
        ;;
    "git")
        case $GITPROT in
            "https")
                GITBASE="https://spolstra@bitbucket.org/spolstra"
                ;;
            "ssl")
                GITBASE="git@bitbucket.org:spolstra"
                ;;
            *)
                # Unknown protocol, exit
                check 1 "Unknown git protocol: $GITPROT"
                ;;
        esac
        get_from_repos
        ;;
    *)
        get_and_unpack_packages
        ;;
esac

# SESAME_ENV_FILE will contain the exports that setup the sesame environment
echo "# Environment setup file generated by $0" > $SESAME_ENV_FILE

setup_mappingmodule
setup_sesame

echo
echo "Installation finished."
echo "Setup up your Sesame environment by sourcing the sesame.env file with:"
echo "source sesame.env"
echo
